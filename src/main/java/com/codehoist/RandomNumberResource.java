package com.codehoist;

import io.micrometer.core.instrument.MeterRegistry;

import java.security.DrbgParameters;
import java.security.NoSuchAlgorithmException;
import java.security.DrbgParameters.Capability;
import java.security.SecureRandom;
import java.util.stream.Collectors;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@Path("/api/v1")
public class RandomNumberResource {

    private static final String NOT_AVAILABLE = "NA";
    private static final Logger LOG = Logger.getLogger(RandomNumberResource.class);
    private final SecureRandom rng;

    @ConfigProperty(name = "codehoist.rng.seed", defaultValue="hello") 
    private String rngSeed;

    private final MeterRegistry metrics;

    public RandomNumberResource(MeterRegistry metrics) {
        this.metrics = metrics;

        SecureRandom drbg = null;
        // The following call requests a strong DRBG instance, with a
        // personalization string. If it successfully returns an instance,
        // that instance is guaranteed to support 256 bits of security strength
        // with prediction resistance available.
        try {
            drbg = SecureRandom.getInstance("DRBG",
                    DrbgParameters.instantiation(256, Capability.PR_AND_RESEED, "hello".getBytes()));
        } catch (final NoSuchAlgorithmException e) {
            LOG.error("Unable to obtain SecureRandomInstance", e);
        }

        rng = drbg != null ? drbg : null;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String generateRandomNumber(@QueryParam("min") @DefaultValue("0") final int min,
            @QueryParam("max") @DefaultValue("10") final int max,
            @QueryParam("quantity") @DefaultValue("1") final int quantity) {

        int maximum = max;
        int minimum = min;
        int count = quantity;

        if(minimum >= maximum) {
            maximum = minimum + 1;
            LOG.debug(String.format("Minimum is greater than or equal to maximum, setting maximum[%d]", maximum));
        }
        
        LOG.debug(String.format("Generating random numbers: min[%d], max[%d], quantity[%d]",minimum,maximum,count));
        String randomValues = NOT_AVAILABLE;
        
        if(rng != null) {
            randomValues = rng.ints(count, minimum, maximum).mapToObj(i -> Integer.toString(i)).collect(Collectors.joining());
        }

        return randomValues;
    }
}
