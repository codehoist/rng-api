package com.codehoist;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.StringRegularExpression.matchesRegex;
@QuarkusTest
public class RandomNumberResourceTest {

    @Test
    public void testRandomEndpoint() {
        given()
          .when().get("/api/v1")
          .then()
             .statusCode(200)
             .body(matchesRegex("-?[0-9]+"));
    }

}
